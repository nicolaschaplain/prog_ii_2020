from threading import Thread
from time import sleep


def worker_1():
    for i in range(0, 5):
        print('.', end='', flush=True)
        sleep(0.5)


def worker_2():
    for i in range(0, 5):
        print('-', end='', flush=True)
        sleep(1)


print('Starting')

# Create read object with reference to worker function
t_1 = Thread(target=worker_1, name='t_1', daemon=False)
t_2 = Thread(target=worker_2, name='t_2', daemon=True)
# Start the thread object
t_1.start()
t_2.start()
# No join
# t_1.join()
# No join
# t_2.join()

print('\nDone')