import concurrent.futures
import time

start = time.perf_counter()


def do_something(seconds):
    print(f'Sleeping {seconds} second(s)...')
    time.sleep(seconds)
    return f'Done Sleeping...{seconds}'


if __name__ == '__main__':
    # https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures

    # .ThreadPoolExecutor If max_workers is None or not given, it will default to the number of
    # processors on the machine, multiplied by 5, assuming that ThreadPoolExecutor is often used
    # to overlap I/O instead of CPU work and the number of workers should be higher than the
    # number of workers for ProcessPoolExecutor.

    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        # Encapsulates the asynchronous execution of a callable. Future instances are created by
        # Executor.submit()
        f1 = executor.submit(do_something, 1)
        f2 = executor.submit(do_something, 1)

        # Return the value returned by the call. If the call hasn’t yet completed then this method
        # will wait up to timeout seconds.
        print(f1.result(timeout=2))  # 0.1 returns TimeOutError
        print(f2.result())

    finish = time.perf_counter()

    print(f'Finished in {round(finish - start, 2)} second(s)')
