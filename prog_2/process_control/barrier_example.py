from threading import Barrier, Thread
from time import sleep
from random import randint


def print_it(msg: str, barrier: Barrier):
    t = randint(1, 20)/100
    print(f'\nExecuting print {msg} with timer: {t}')
    for i in range(0, 10):
        print(msg, end='', flush=True)
        sleep(t)
    print(f'\nPrinting end: {msg}')
    barrier.wait()
    print(f'\nReturning from print_it: {msg}')


def callback():
    print('\nCallback Executing')


def main():
    print('Main - Starting')

    barrier = Barrier(4, callback)
    t1 = Thread(target=print_it, args=('A', barrier))
    t2 = Thread(target=print_it, args=('B', barrier))
    t3 = Thread(target=print_it, args=('C', barrier))
    t1.start()
    t2.start()
    t3.start()
    barrier.wait()
    print('\nMain - Done')


if __name__ == '__main__':
    main()
