from http.server import BaseHTTPRequestHandler, ThreadingHTTPServer
from urllib import parse


PRICES = {
    "70": "160",
    "60": "140",
    "50": "100"
}


class PriceHttpRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        print("do_GET() starting to process request")
        query = parse.urlparse(self.path).query
        query_components = dict(qc.split("=") for qc in query.split("&"))
        rooms = query_components["rooms"]
        price = PRICES.get(rooms)
        byte_msg = bytes(price, 'utf-8')
        self.send_response(200)
        self.send_header("Content-type", 'text/plain; charset-utf-8')
        self.send_header('Content-length', str(len(byte_msg)))
        self.end_headers()
        print(f'do_GET() replying with room price: {price}')
        self.wfile.write(byte_msg)


def main():
    print('Setting up server')
    server_address = ('localhost', 8080)
    httpd = ThreadingHTTPServer(server_address, PriceHttpRequestHandler)
    print('Activating HTTP server')
    httpd.serve_forever()


if __name__ == '__main__':
    main()
