from dataclasses import dataclass


@dataclass
class Graduate:
    name: str
    gpa: float

    def __lt__(self, other):
        return self.gpa < other.gpa


graduates = [Graduate('john', 4.95), Graduate('mary', 5.0), Graduate('lucy', 4.0)]
valedictorian = max(graduates)
print(valedictorian.name)