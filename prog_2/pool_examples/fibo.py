from concurrent.futures import ProcessPoolExecutor


def fibonacci(x: int) -> int:
    if (x <= 1):
        return x
    else:
        return fibonacci(x - 1) + fibonacci(x - 2)

if __name__ == '__main__':
    with ProcessPoolExecutor(4) as pool:
        processed_list = pool.map(fibonacci, range(20))

    print(list(processed_list))