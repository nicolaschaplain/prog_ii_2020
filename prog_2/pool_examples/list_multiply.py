from concurrent.futures import ProcessPoolExecutor

def mul(x, y):
    return x * y

with ProcessPoolExecutor(max_workers=4) as pool:
    results = pool.map(mul, range(0, 100, 2), range(1, 101, 2))


print(list(results))