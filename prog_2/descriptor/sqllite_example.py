import sqlite3

conn = sqlite3.connect('entertainment.db')

c = conn.cursor()

# Create table
c.execute('''CREATE TABLE IF NOT EXISTS Movies
             (title text, director text, year int)''')

c.execute('''CREATE TABLE IF NOT EXISTS Music
             (title text, year int, genre text, artist text)''')

# Insert a row of data
c.execute("INSERT INTO  Movies VALUES ('Star Wars','George Lucas', 1977)")
c.execute("INSERT INTO Movies VALUES ('Jaws','Steven Spielberg', 1975)")
c.execute("INSERT INTO Music VALUES ('Country Roads', 1971, ' Country music, Country',"
          " 'John Denver')")

# Save (commit) the changes
conn.commit()

# We can also close the connection if we are done with it.
# Just be sure any changes have been committed or they will be lost.
conn.close()
