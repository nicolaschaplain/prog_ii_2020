import logging

logging.basicConfig(level=logging.INFO)


class LoggedAccess:

    def __set_name__(self, owner, name):
        self.public_name = name
        self.private_name = f'_{name}'

    def __get__(self, instance, owner=None):
        value = getattr(instance, self.private_name)
        logging.info('Accessing %r giving %r', self.public_name, value)
        return value

    def __set__(self, instance, value):
        logging.info('Updating %r to %r', self.public_name, value)
        setattr(instance, self.private_name, value)


class Person:
    name = LoggedAccess()  # First descriptor
    age = LoggedAccess()  # Second descriptor

    def __init__(self, name, age):
        self.name = name  # Calls the first descriptor
        self.age = age  # Calls the second descriptor

    def birthday(self):
        self.age += 1


# An interactive session shows that the Person class has called __set_name__() so that the field
# names would be recorded. Here we call vars() to lookup the descriptor without triggering it:

vars(vars(Person)['name'])

vars(vars(Person)['age'])

# The new class now logs access to both name and age:

pete = Person('Peter P', 10)
kate = Person('Catherine C', 20)

# The two Person instances contain only the private names:

vars(pete)
vars(kate)
