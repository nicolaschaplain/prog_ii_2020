import logging
from email_validator import validate_email, EmailNotValidError
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()

email = "my+address@mydomain.tld"

logger.debug(f"Checking email: {email}")
try:
    # Validate.
    valid = validate_email(email)
    logger.info(f"Validated email: {valid.email}")
    # Update with the normalized form.
    email = valid.email
except EmailNotValidError as e:
    # email is not valid, exception message is human-readable
    logger.warning(f"Bad email: {email}")
    print(str(e))
