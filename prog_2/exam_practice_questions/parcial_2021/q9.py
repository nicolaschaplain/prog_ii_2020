import names
from dataclasses import dataclass
import random


@dataclass
class Graduate:
    name: str
    gpa: float


graduate_list = [Graduate(
    name=names.get_full_name(),
    gpa=round(random.random() * 5, 1)
) for _ in range(101)]

list(map(lambda s: (s.name, s.gpa),
         filter(lambda s: s.gpa >= 4.5, graduate_list)))


@dataclass
class Graduate:
    name: str
    courses: set[str]


courses = ['prog I', 'prog II', 'algo', 'dss', 'ml', 'ai']

graduate_list = [Graduate(
    name=names.get_full_name(),
    courses=set(random.choices(courses, k=2))
) for _ in range(20)]

list(map(lambda s: (s.name, s.courses),
         filter(lambda s: 'prog II' in s.courses, graduate_list)))
