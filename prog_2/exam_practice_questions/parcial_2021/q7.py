from collections import UserDict, ChainMap


class Cart(UserDict):

    def __add__(self, other: 'Cart') -> ChainMap:
        return ChainMap(self, other)


cart_1 = Cart(pen=1, pencil=2)
cart_2 = Cart(rubber=2, paper=8)

total = cart_1 + cart_2

print(total['pencil'])
