import concurrent.futures
import time


def power_2(x: int) -> int:
    print('Compute power_2...')
    time.sleep(2)
    print('power_2 complete...')
    return x ** 2


def power_3(x: int) -> int:
    print('Compute power_3...')
    time.sleep(3)
    print('power_3 complete...')
    return x ** 3


def f() -> int:
    with concurrent.futures.ThreadPoolExecutor(2) as pool:
        r_2 = pool.submit(power_2, 2)
        r_3 = pool.submit(power_3, 3)
    return r_2.result() + r_3.result()


if __name__ == '__main__':
    print(f())
