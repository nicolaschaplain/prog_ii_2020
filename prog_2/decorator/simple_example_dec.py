from typing import Callable


def logged(func: Callable) -> Callable:
    def inner(x, y):
        print('calling ', func.__name__)
        assert isinstance(x, int)
        assert isinstance(y, int)
        result = func(x, y)
        print('called ', func.__name__)
        return result
    return inner


@logged
def multiplication(x: int, y: int):
    return x * y


print(multiplication(4, 4))
