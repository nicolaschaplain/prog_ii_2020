class Article:

    def __init__(self, article_id):
        self.article_id = article_id
        self._price = None

    @property
    def price(self):
        """Article price"""
        return self._price

    @price.setter
    def price(self, new_price):
        if isinstance(new_price, float) and new_price > 0:
            self._price = new_price
        else:
            print("Please enter a valid price")

    @price.deleter
    def price(self):
        del self._price

    def __str__(self):
        if hasattr(self, '_price'):
            return f"Article['{self.article_id}', {self._price}]"
        else:
            return f"Article['{self.article_id}']"


if __name__ == '__main__':
    art = Article('0001')
    art.price = 'a'
    art.price = 2.3
    print(art)
    del art.price
    print(art)
