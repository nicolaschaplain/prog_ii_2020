import numpy as np


def objective_sum(a: np.array, arr_size: int,
                  target_sum: int) -> bool:
    a = np.sort(a, kind='quicksort')
    l = 0
    r = arr_size - 1
    while l < r:
        if a[l] + a[r] == target_sum:
            return True
        elif a[l] + a[r] < target_sum:
            l += 1
        else:
            r -= 1
    return False


def check_objective_sum(a: np.array, n: int) -> None:
    if objective_sum(a, len(a), n):
        print("Array has two elements with the given"
              "target_sum")
    else:
        print("Array doesn't have two elements "
              "with the given target_sum")


input_array = np.array([1, 4, 45, 6, 10, -8])
target_n = 16
check_objective_sum(input_array, target_n)
