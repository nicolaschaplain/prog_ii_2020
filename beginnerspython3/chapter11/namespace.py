# Example taken from https://code.tutsplus.com/tutorials/what-are-python-namespaces-and-why-are
# -they-needed--cms-28598

from datetime import datetime

A_CONSTANT = 3


def print_counter():
    counter = 3
    print(datetime.now())
    print(dir())
    print(counter)


print_counter()
print(dir())
