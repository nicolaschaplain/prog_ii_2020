from functools import partial


def multiply(a, b):
    return a * b


print(multiply(2, 5))

double = partial(multiply, 2)       # Partial evaluation
doubled = double(3)                 # Currying

print(doubled)

# https://toolz.readthedocs.io/en/latest/curry.html