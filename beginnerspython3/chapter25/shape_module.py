"""This is a test module"""

print('Hello I am the utils module')


def printer(some_object):
    # This is a function that prints an object
    print('printer')
    print(some_object)
    print('done')


class Shape:
    # This is a class
    def __init__(self, shape_id):
        self._id = shape_id


# This is an object of class Shape
default_shape = Shape('square')


def _special_function():
    print('Special function')
