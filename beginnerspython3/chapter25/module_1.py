"""This is a test module"""

print('Hello I am module 1')


def f():
    return 'This is the output of function f of module module_1'


class A:
    def __init__(self):
        self.a = 42
        self.b = list(self.a + i for i in range(10))


if __name__ == '__main__':
    x = 1 + 2
    print('x is', x)
    f()
