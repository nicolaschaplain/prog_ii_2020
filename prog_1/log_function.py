import math

def log_func(w: float, t: float) -> callable:

    def inner(x: float) -> float:
        if x < -100:
            return 0
        return 1 / (1 + math.exp(- w * (x - t)))

    return inner


f = log_func(1, 3)

f(3)
