def marked_print(user_name: str) -> callable:
    def inner(text: str):
        print(f'{user_name}: {text}')

    return inner


user_printer = marked_print('john')
user_printer('This is a text')