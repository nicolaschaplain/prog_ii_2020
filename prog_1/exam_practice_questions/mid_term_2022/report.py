import uuid
import datetime as dt


class Report:
    def __init__(self, client_id: str):
        self.report_id = uuid.uuid4()
        self.client_id = client_id

    def __str__(self):
        return f'Client [{self.client_id}] Report ID [{self.report_id}]'


class MaintenanceReport(Report):

    def __init__(self, client_id: str, note: str):
        super().__init__(client_id)
        self.date = dt.datetime.now()
        self.note = note

    def replace_note(self, new_note: str):
        self.date = dt.datetime.now()
        self.note = new_note

    def __str__(self):
        return f'Client [{self.client_id}] Report ID [{self.report_id}]: {self.note}'


report = MaintenanceReport('3332', '25000 km vehicle maintenance requested')
print(report)
print(report.date)

report.replace_note('25000 km maintenance completed')
print(report)
print(report.date)
