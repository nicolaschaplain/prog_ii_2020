# If we don't use the global keywords, we are creating a new local varaible

def outer():
    def inner():
        a_variable = 7
        print(a_variable)

    a_variable = 2
    print(a_variable)
    inner()
    print(a_variable)


a_variable = 10
outer()
print(a_variable)
