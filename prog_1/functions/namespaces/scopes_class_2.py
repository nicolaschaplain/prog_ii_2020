from typing import Final

A_CONSTANT: Final = 3


def printing():
    n = 0

    def calcualtion(x, n) -> tuple[int, int]:
        n += 1
        y = x ** 2
        return y, n

    def calcualtion_2(x, n) -> tuple[int, int]:
        n += 1
        y = x ** 2
        return y, n

    z, n = calcualtion(3, n)
    w, n = calcualtion_2(5, n)

    print(z, w)
    print(n)


printing()