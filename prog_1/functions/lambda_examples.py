student_tuples = [
    ('john', 'A', 15),
    ('jane', 'B', 12),
    ('dave', 'B', 10),
]

print(sorted(student_tuples, key=lambda student: student[2]))
print(sorted(student_tuples, key=lambda student: (student[1], student[2])))


def sorting_criteria(x: tuple[str, str, int]) -> int:
    return x[2]


print(sorted(student_tuples, key=sorting_criteria))
