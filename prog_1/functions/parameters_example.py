def make_list_of_range(start: int, end: int) -> list:
    """
    Makes a list of consecutive integers between start and end
    :param start:   first integer
    :param end:     last ingeter
    :return:        a list with consecutive integers
    """

    new_list = list(range(start, end))
    return new_list


make_list_of_range(2, 4)
make_list_of_range(start=2, end=4)
make_list_of_range(end=2, start=4)
make_list_of_range(2, end=4)


def make_list_of_range_default(start: int, end: int = 10) -> list:
    """
    Makes a list of consecutive integers between start and end
    :param start:   first integer
    :param end:     last ingeter
    :return:        a list with consecutive integers
    """

    newList = list(range(start, end))
    return newList


make_list_of_range_default(2)


def greeter(*names: str) -> None:
    for name in names:
        print('Welcome', name)


greeter('John', 'Denise', 'Phoebe', 'Adam', 'Gryff', 'Jasmine')


def sorted_greeter(*names: str) -> None:
    print(f'Sorted names: {sorted(names)}')


sorted_greeter('John', 'Denise', 'Phoebe', 'Adam', 'Gryff', 'Jasmine')


def values_to_lowercase(**kwargs: str):
    for key, value in kwargs.items():
        print(f"wisper {key} value, {value.lower()}")


values_to_lowercase(first='I', mid='SAY', last='SHOUT')
