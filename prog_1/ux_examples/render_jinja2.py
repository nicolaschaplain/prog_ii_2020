from jinja2 import Template
import flask
template = Template('Hello {{ name }}!')
template.render(name='John Doe')

app = flask.Flask(__name__)

with app.app_context():
    a = flask.render_template('child_example.html')
