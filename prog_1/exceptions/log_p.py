import datetime as dt
from math import log
from dataclasses import dataclass


@dataclass
class DomainError(Exception):
    msg: str
    date_of_error: dt.datetime


@dataclass
class LogDomainError(DomainError):
    pass

def f(x: float) -> float:
    if x <= 0:
        raise LogDomainError('out of domain for log function', dt.datetime.now())
    y = log(x)
    print('log computed')
    return y


try:
    print(f(-2))
except DomainError as exp:
    print(f'{exp.msg} at datetime: {exp.date_of_error}')
except TypeError:
    print('error in type')
