from collections import deque

# FIFO, use a conventional list

queue = deque(["Eric", "John", "Michael"])

queue.append("Terry")           # Terry arrives
queue.append("Graham")          # Graham arrives
queue.popleft()                 # The first to arrive now leaves
queue.popleft()                 # The second to arrive now leaves

# LIFO, use a conventional list

stack = [3, 4, 5]

stack.pop()
stack.append(6)
stack.append(7)
stack.pop()

stack.sort()