import re

pattern = '^a...s$'
test_string = 'abyss'
result = re.match(pattern, test_string)

if result is None:
    print("Search successful.")
else:
    print("Search unsuccessful.")
