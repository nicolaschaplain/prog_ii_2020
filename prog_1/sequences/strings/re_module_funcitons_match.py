import re

mo = re.match(r"c", "abcdef")  # No match

if not mo:
    print('No match')

# Finds c anywhere in the string
mo2 = re.search(r"c", "abcde")  # Match


# Finds a group
mo3 = re.search(r"(b?c)", "abcde")  # Match

# The groups (only one group in this case)
mo3.groups()

# Extract group 1
mo3.group(1)

# Span (start and end of the group
mo3.span(1)

# Defining two groups

mo4 = re.search("(b?c).*(e?e)", "abcde")  # Match

# The groups (only one group in this case)
mo4.groups()

# Extract group 1
mo3.group(1)

# Span (start and end of the group
mo3.span(1)