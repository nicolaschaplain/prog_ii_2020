import re

result = re.match('[abc]', 'a fruit')

result.span()

m = re.match(r'(.*bc)def', 'abcdef')
m.group(1)

m = re.match(r'(.+) \1', 'the the')
m.group(1)

m = re.match(r'(.+) \1', 'thethe')
print(m is None)

re.match("c", "abcdef")    # No match
re.search("c", "abcdef")   # Match


re.split(r'\W+', 'Words, words, words.')

re.split(r'(\W+)', 'Words, words, words.')

re.split(r'\W+', 'Words, words, words.', 1)

re.split('[a-f]+', '0a3B9', flags=re.IGNORECASE)

text = """Ross McFluff: 834.345.1254 155 Elm Street
Ronald Heathmore: 892.345.3428 436 Finley Avenue
Frank Burger: 925.541.7625 662 South Dogwood Way
Heather Albrecht: 548.326.4584 919 Park Place"""

entries = re.split("\n+", text)

[re.split(":? ", entry, 4) for entry in entries]