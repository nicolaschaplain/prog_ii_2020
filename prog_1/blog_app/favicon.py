import os
from flask import Flask, current_app, send_from_directory

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/favicon.ico', methods=['GET'])
def favicon():
    return send_from_directory(os.path.join(current_app.root_path, 'static'),
                               'music.png',
                               mimetype='image/png')


if __name__ == '__main__':
    app.run()






