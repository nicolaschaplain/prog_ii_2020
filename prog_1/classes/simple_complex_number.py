class Complex:
    def __init__(self, r: float, i: float):
        self.x = r
        self.y = i

    def modulo(self):
        return (self.x ** 2 + self.y ** 2) ** 0.5

    def __str__(self):
        return f'{self.x} + {self.y}j'


u = Complex(1, 2)
u.modulo()
print(u)


class AdminUser:
    def __init__(self, name: str, email: str):
        self.name = name
        self.email = email
        self.domain = 'ufv.es'

    def print_user_name(self, title: str):
        print(f'My name is {title} {self.name} and my email is {self.email}')


u1 = AdminUser('Mary', 'mary@ufv.es')
u2 = AdminUser('John', 'john@ufv.es')
u1.name

print(u1.name)
print(u2.email)

u1.print_user_name('Mrs')

class Dog:
    """A dog class"""
    def __init__(self, name: str):
        self.name = name
        self.tricks = []

    def add_trick(self, *trick: str):
        self.tricks.extend(trick)

    def __str__(self):
        if len(self.tricks) > 0:
            return f'Dog: {self.name}, with tricks: {self.tricks}'
        else:
            return f'Dog: {self.name}'

    def __repr__(self):
        return f'Dog("{self.name}")'

bobby = Dog('bobby')

Dog.__name__
Dog.__dict__
Dog.__doc__