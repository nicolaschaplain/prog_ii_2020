# A simple class. f is a statick method, but we keep it simple

class MyClass:
    """A simple example class"""
    def f(self):
        return 'hello world'


x = MyClass()
x.f()

y = MyClass()

id(x)
id(y)