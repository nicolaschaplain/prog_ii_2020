# Expression statement, executed interactively

# >>> 3 + 4

print("Hi")

# formatted string literal

year = 2016
event = 'Referendum'
f'Results of the {year} {event}'

# The str.format() method of strings requires more manual effort. You’ll still use { and }
# to mark where a variable will be substituted and can provide detailed formatting directives,
# but you’ll also need to provide the information to be formatted.

yes_votes = 42_572_654
no_votes = 43_132_495
percentage = yes_votes / (yes_votes + no_votes)
'{:-9} YES votes  {:2.2%}'.format(yes_votes, percentage)

# More examples (https://docs.python.org/3/library/string.html#formatstrings):

# Accessing items

coord = (3, 5)
'X: {0[0]};  Y: {0[1]}'.format(coord)

# Aligning the text and specifying a width:
'{:<30}'.format('left aligned')
'{:>30}'.format('right aligned')
'{:^30}'.format('centered')
'{:*^30}'.format('centered')  # use '*' as a fill char

# Fixed point notation
'{:+f}; {:+f}'.format(3.14, -3.14)  # show it always
'{: f}; {: f}'.format(3.14, -3.14)  # show a space for positive numbers
'{:-f}; {:-f}'.format(3.14, -3.14)  # show only the minus -- same as '{:f}; {:f}'

# Expressing %

points = 19
total = 22
'Correct answers: {:.2%}'.format(points/total)

# Date time